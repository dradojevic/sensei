-- DATABASE SCHEMA FOR sensei

-- Entities BEGIN

DROP TABLE IF EXISTS "session" CASCADE;

CREATE TABLE "session"(
       "id" SERIAL NOT NULL,
       "description" text,
       "ip" inet NOT NULL,
       "created_at" timestamptz NOT NULL,
       PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "sensor" CASCADE;

CREATE TABLE "sensor"(
		"id" SERIAL NOT NULL,
		"type" int2 NOT NULL,
		"uid" text NOT NULL,
		PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "reading" CASCADE;

CREATE TABLE "reading"(
       "id" SERIAL NOT NULL,
       "session_id" int4 NOT NULL,
       "sensor_id" int4 NOT NULL,
       "reading" float NOT NULL,
       "measured_at" timestamptz NOT NULL,
       PRIMARY KEY("id")
);

-- Entities END

-- References BEGIN

ALTER TABLE "reading" ADD CONSTRAINT "Ref_reading_to_session" FOREIGN KEY ("session_id")
      REFERENCES "session"("id")
      MATCH SIMPLE
      ON DELETE CASCADE
      ON UPDATE NO ACTION
      NOT DEFERRABLE;

ALTER TABLE "reading" ADD CONSTRAINT "Ref_reading_to_sensor" FOREIGN KEY ("sensor_id")
      REFERENCES "sensor"("id")
      MATCH SIMPLE
      ON DELETE CASCADE
      ON UPDATE NO ACTION
      NOT DEFERRABLE;

-- References END

-- Indexes BEGIN

CREATE INDEX "reading_session_id_sensor_id_timestamp_idx" ON "reading"("session_id", "sensor_id", "measured_at");

-- Indexes END