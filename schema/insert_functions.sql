CREATE OR REPLACE FUNCTION "public"."insert_session" (
    text, -- description ($1)
    inet  -- ip ($2)
) RETURNS int4 AS
$body$
DECLARE
	new_id int4;
BEGIN
	INSERT INTO session(
	    description,
	    ip,
	    created_at)
	VALUES (
	    $1, -- description
	    $2, -- ip
	    now() -- created_at
	) RETURNING id INTO new_id;

	RETURN new_id;
END;
$body$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION "public"."insert_sensor" (
		text, -- uid ($1)
		int2 -- type ($2)
) RETURNS int4 AS
$body$
DECLARE
	sensor_id int4;
BEGIN

	-- first try to find that sensor
	SELECT INTO sensor_id id FROM sensor WHERE uid = $1;
	IF sensor_id IS NULL THEN
		
		-- it does not exist, insert it
		INSERT INTO sensor(
			uid,
			type
		) VALUES (
			$1, -- uid
			$2 -- type
		) RETURNING id INTO sensor_id;
	END IF;
	
	RETURN sensor_id;
END;
$body$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION "public"."insert_reading" (
	int4, -- session_id
	int4, -- sensor_id
	float, -- reading
	int8 -- measured_at
) RETURNS void AS
$body$
BEGIN
	INSERT INTO reading(
		session_id,
		sensor_id,
		reading,
		measured_at
	) VALUES (
		$1, -- session_id
		$2, -- sensor_id
		$3, -- reading
		to_timestamp($4::double precision / 1000) -- measured_at
	);
END;
$body$
LANGUAGE 'plpgsql';