#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''sensei.py: Sensei is remote sensing server.

Sensei receives remote readings (temperature/current/voltage) and
stores them in database. It also shows the readings on a web page
as pretty graphs.'''

__author__ = 'Dimitrije Radojevic'
__copyright__ = 'Copyright 2014 Dimitrije Radojevic'
__email__ = 'templaryum@gmail.com'

from json import dumps
import sys
import traceback
from logging import getLogger

from flask import *
import psycopg2.extras
import psycopg2.pool

from gviz_api import gviz_api
from sensei_protobuf.generated.SensorDefinition_pb2 import InitSessionRequest, InitSessionResponse, \
    PostReadingRequest, PostReadingResponse
from sensei_util.log.Logger import Logger

MODULE = 'sensei'
'''Module name.'''

_conn_pool = None
'''Global connection pool.'''


def init():
    """Configure and initialize main Flask app."""
    global app, reading_table_description

    # Create our app
    app = Flask(MODULE)

    app.config.from_object(MODULE)

    # Load default config
    app.config.update(dict(
        DB_NAME='sensei',
        DB_HOST='localhost',
        DB_USER='dimitrijer',
        DB_PASSWORD='qwe123',
        DEBUG=True,
        SECRET_KEY='mijedobiogrip',
        USERNAME='admin',
        PASSWORD='default'
    ))

    # Load more config/override config from env variable, if set
    app.config.from_envvar('SENSEI_SETTINGS', silent=True)

    # Initialize logging system
    Logger.init_logging(
        loggers=[app.logger, getLogger('werkzeug')],
        log_trace=True)

    # ... and connect to DB
    init_db()

    # Describe table data structure for graph
    reading_table_description = {"measured_at": ("datetime", "time"),
                                 "reading": ("number", "reading")}


def init_db():
    '''
    Initialize psycopg connection pool and connect to the Postgres
    database.
    '''
    global _conn_pool
    try:
        _conn_pool = psycopg2.pool.SimpleConnectionPool(
            1,
            3,
            database=app.config['DB_NAME'],
            user=app.config['DB_USER'],
            password=app.config['DB_PASSWORD'],
            host=app.config['DB_HOST'],
            port=5433)

    except Exception:
        app.logger.exception('Failed to instantiate connection pool!')
        sys.exit(1)

# We need to call init prior to defining view methods
init()


def get_db_conn():
    '''
    Returns request-context database connection. Opens a new connection for
    each request.
    '''
    if not hasattr(g, 'postgres_conn'):
        g.postgres_conn = _conn_pool.getconn()
    return g.postgres_conn


@app.teardown_request
def release_db_conn(error):
    '''
    Releases database connection back to the pool.
    '''
    postgres_conn = getattr(g, 'postgres_conn', None)
    if postgres_conn is not None:
        _conn_pool.putconn(postgres_conn)


def shutdown_server():
    '''
    Shuts down the server in a clean way. Requires request context.
    '''
    app.logger.debug('Shutting down server.')
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server!')
    func()
    # Shutdown connection pool
    _conn_pool.closeall()


def rows_to_json(cur):
    '''
    Converts rows returned by cursor to JSON, appropriate for returning via
    AJAX to JS.
    '''
    r = [dict((cur.description[i][0], value) for i, value in enumerate(row))
         for row in cur.fetchall()]
    return dumps(r)


@app.route('/login', methods=['GET', 'POST'])
def login():
    '''
    Logs the user in with provided username/password.
    '''
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            app.logger.warn(
                'Invalid username supplied (IP: %s)!' %
                (request.remote_addr))
            error = 'Invalid username!'
        elif request.form['password'] != app.config['PASSWORD']:
            app.logger.warn(
                'Invalid password supplied (IP: %s)!' %
                (request.remote_addr))
            error = 'Invalid password!'
        else:
            session['logged_in'] = True
            flash('You were successfully logged in!')
            app.logger.debug('User logged in.')
            return redirect(url_for('show_readings'))

    return render_template('login.html', error=error)


@app.route('/logout', methods=['GET'])
def logout():
    '''
    Logs the user out.
    '''
    session.pop('logged_in', None)
    flash('You were logged out!')
    app.logger.debug('User logged out.')
    return redirect('/')


@app.route('/')
def show_home():
    '''
    Shows login form if user is not logged in, or readings page.
    '''
    if 'logged_in' not in session:
        return redirect(url_for('login'))
    else:
        return redirect(url_for('show_readings'))


@app.route('/sensor/init', methods=['POST'])
def init_session():
    '''
    Inits session with Seito.
    '''
    try:
        # Parse incoming proto
        message = InitSessionRequest()
        message.ParseFromString(request.data)

        # Obtain a cursor
        cur = get_db_conn().cursor()

        # Try to insert each sensor, if it doesn't exist
        for sensor in message.sensors:
            cur.execute("SELECT * FROM insert_sensor(%s, %s::int2)",
                        [sensor.uid, sensor.type])

            # Set sensor id in proto
            sensor.id = cur.fetchone()[0]

        # Create a new session
        cur.callproc(
            "insert_session", [
                message.sessionDescription, request.remote_addr])
        sessionId = cur.fetchone()[0]

        # Commit transaction and dispose of cursor
        get_db_conn().commit()
        cur.close()

        # Log a bit
        print 'Initiated session %d from %s.' % (sessionId,
                                                 request.remote_addr)
        print 'Sensors:'
        for sensor in message.sensors:
            print 'ID: %d, UID: %s, type: %d' % (sensor.id, sensor.uid,
                                                 sensor.type)

        # Create and send response
        response = InitSessionResponse()
        response.sessionId = sessionId
        response.sensors.extend(message.sensors)

        return response.SerializeToString()
    except Exception:
        print 'Failed to init session from ' + request.remote_addr + '!'
        traceback.print_exc(file=sys.stdout)
        abort(500)


@app.route('/sensor/reading', methods=['POST'])
def post_reading():
    """This is called by the sensor, which sends recent readings as a
    protobuf message."""
    try:
        # Parse incoming proto
        message = PostReadingRequest()
        message.ParseFromString(request.data)

        # Obtain a cursor
        cur = get_db_conn().cursor()

        # Insert each reading
        readingCount = 0
        for reading in message.readings:
            cur.callproc("insert_reading",
                         [message.sessionId,
                          reading.sensorId,
                          reading.reading,
                          reading.timestamp])
            readingCount += 1

        # Commit transaction and dispose of cursor
        get_db_conn().commit()
        cur.close()

        print 'Posted %d readings by session ID %d' % (readingCount,
                                                       message.sessionId)

        # return an empty response
        return PostReadingResponse().SerializeToString()
    except Exception:
        print 'Failed to insert readings!'
        traceback.print_exc(file=sys.stdout)
        abort(500)


@app.route('/query/readings')
def get_readings():
    """Gets a list of readings from specific sensor in specific session."""
    try:
        # Check if session and sensor id are defined
        sessionId = request.args.get('sessionId', 0, type=int)
        sensorId = request.args.get('sensorId', 0, type=int)
        if not sessionId or not sensorId:
            print 'Session ID or sensor ID not defined, invalid request!'
            abort(500)

        # Obtain a cursor
        cur = get_db_conn().cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Get a list of all readings from this sensor during this session
        cur.execute('SELECT reading, measured_at FROM reading WHERE '
                    'session_id = %s AND sensor_id = %s',
                    [sessionId, sensorId])
        rows = cur.fetchall()

        # Dispose of cursor
        cur.close()

        # Build data dictionary
        graph_data = []
        for entry in rows:
            graph_entry = {'reading': entry['reading'],
                           'measured_at': entry['measured_at']}
            graph_data.append(graph_entry)

        print 'Loaded', len(graph_data), 'readings.'

        # Load data to Google's data tble structure
        data_table = gviz_api.DataTable(reading_table_description)
        data_table.LoadData(graph_data)

        # Return a JSON
        return data_table.ToJSon()
    except Exception:
        print "Failed to load readings!"
        traceback.print_exc(file=sys.stdout)
        abort(500)


@app.route('/query/sensors')
def get_sensors():
    """Gets a list of sensors for given session."""
    try:
        # Check if session id is defined
        sessionId = request.args.get('id', 0, type=int)
        if not sessionId:
            print 'Session ID not defined, invalid request!'
            abort(500)

        # Obtain a cursor
        cur = get_db_conn().cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Get a list of all sensors that appear in readings from this session
        cur.execute('SELECT id, uid, type FROM sensor WHERE id IN '
                    '(SELECT DISTINCT sensor_id FROM reading WHERE '
                    'session_id = %s)', [sessionId, ])

        json_data = rows_to_json(cur)

        # Dispose of cursor
        cur.close()

        # Return list of sensors
        return json_data
    except Exception:
        print "Failed to load sensors!"
        traceback.print_exc(file=sys.stdout)
        abort(500)


@app.route('/show_readings')
def show_readings():
    """Fetches readings from the database and shows them on
    page as a nice graph."""

    # Make sure the user is logged in
    if session.get('logged_in'):

        try:
            # Obtain a cursor
            cur = get_db_conn().cursor(
                cursor_factory=psycopg2.extras.DictCursor)

            # Get a list of all sessions
            cur.execute('SELECT id, description, ip, created_at FROM session')
            rows = cur.fetchall()

            # Dispose of cursor
            cur.close()

            return render_template('show_readings.html', sessions=rows)
        except Exception:
            app.logger.error('Failed to fetch readings!', exc_info=1)
            abort(500)
    else:
        return redirect(url_for('login'))

if __name__ == '__main__':
    # Run the main loop
    app.run(host='0.0.0.0')
